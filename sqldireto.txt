
### Criando um banco de dados.
$ sqlite3 Clientes.db

sqlite> .help 
Criando a tabela clientes. 
sqlite> CREATE TABLE clientes(
   ...> id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   ...> Nome VARCHAR(100) NOT NULL,
   ...> CPF VARCHAR(11) NOT NULL,
   ...> Email VARCHAR(20) NOT NULL,
   ...> Fone VARCHAR(20),
   ...> UF VARCHAR(2) NOT NULL
   ...> );

### Visualizando o código SQL que criou a tabela.
sqlite> .schema clientes

### Visualizando todas as tabelas existentes.
sqlite> .table

### Saindo do SQLite3.
sqlite> .exit

### Inserindo dados
INSERT INTO clientes VALUES(1, 'Regis', '00000000000', 'rg@email.com', '1100000000', 'SP');
INSERT INTO clientes VALUES(2, 'Abigail', '11111111111', 'abigail@email.com', '1112345678', 'RJ');
INSERT INTO clientes VALUES(3, 'Benedito', '22222222222', 'benedito@email.com', '1187654321', 'SP');
INSERT INTO clientes VALUES(4, 'Zacarias', '33333333333', 'zacarias@email.com', '1199999999', 'RJ');

### Visualizando os dados.
$ sqlite3 Clientes.db
sqlite> SELECT * FROM clientes;